<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from person";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<center><h1>Datos del aspirante y Docmentos Consignados<h1></center>
	<thead>
	<th>Nombre</th>
	<th>Apellido</th>
	<th>Fecha de Nacimiento</th>
	<th>Genero</th>
	<th>Cedula</th>
	<th>Copia partida nacimiento</th>
	<th>Copia cedla</th>
	<th>Copia titlo de bachiller</th>
	<th>Copia certificacion de notas</th>
	<th>Copia Ops</th>
	<th>Copia Fondo Negro</th>
	<th></th>
	</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["name"]; ?></td>
	<td><?php echo $r["lastname"]; ?></td>
	<td><?php echo $r["birthdate"]; ?></td>
	<td><?php echo $r["gender"]; ?></td>
	<td><?php echo $r["cedula"]; ?></td>
	<td><?php echo $r["c_partida_nacimiento"]; ?></td>
	<td><?php echo $r["ci_copia"]; ?></td>
	<td><?php echo $r["c_titulo_bachiller"]; ?></td>
	<td><?php echo $r["c_certificacion_calificacione"]; ?></td>
	<td><?php echo $r["c_opsu"]; ?></td>
	<td><?php echo $r["c_fondo_negro"]; ?></td>
	<td style="width:150px;">
		<a data-id="<?php echo $r["id"];?>" class="btn btn-edit btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["id"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["id"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro Que desea eliminar los recuados?");
			if(p){
				$.get("./php/eliminar.php","id="+<?php echo $r["id"];?>,function(data){
					loadTabla();
				});
			}

		});
		</script>
	</td>
</tr>
 
<?php endwhile;?>
<?php else:?>
	<h1><p style="color:black" class="alert alert-warning">Recordar Agregar 2 veces para mostrar registros y modificarlos si lo desea </p><h1>
<?php endif;?>
<script>
  	$(".btn-edit").click(function(){
  		id = $(this).data("id");
  		$.get("./php/formulario.php","id="+id,function(data){
  			$("#form-edit").html(data);
  		});
  		$('#editModal').modal('show');
  	});
  </script>
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Actualizar</h4>
        </div>
        <div class="modal-body">
        <div id="form-edit"></div>
        </div>

      </div>
    </div>
  </div>