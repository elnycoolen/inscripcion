<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from person where name like '%$_GET[s]%' or lastname like '%$_GET[s]%' or gender like '%$_GET[s]%' or birthdate like '%$_GET[s]%' or cedula like '%$_GET[s]%' or c_cedula_indentidad like '%$_GET[s]%' or c_partida_nacimiento like '%$_GET[s]%' or c_titulo_bachiller like '%$_GET[s]%' or c_certificacion_calificacione like '%$_GET[s]%' or c_opsu like '%$_GET[s]%' or c_fondo_negro like '%$_GET[s]%' ";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<center><h1>Datos del aspirante y Docmentos Consignados<h1></center>
	<thead>
	<th>Nombre</th>
	<th>Apellido</th>
	<th>Fecha de Nacimiento</th>
	<th>Genero</th>
	<th>Cedula</th>
	<th>Copia partida nacimiento</th>
	<th>Copia cedla</th>
	<th>Copia titlo de bachiller</th>
	<th>Copia certificacion de notas</th>
	<th>Copia Ops</th>
	<th>Copia Fondo Negro</th>
	<th></th>
	</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["name"]; ?></td>
	<td><?php echo $r["lastname"]; ?></td>
	<td><?php echo $r["birthdate"]; ?></td>
	<td><?php echo $r["gender"]; ?></td>
	<td><?php echo $r["cedula"]; ?></td>
	<td><?php echo $r["c_partida_nacimiento"]; ?></td>
	<td><?php echo $r["ci_copia"]; ?></td>
	<td><?php echo $r["c_titulo_bachiller"]; ?></td>
	<td><?php echo $r["c_certificacion_calificacione"]; ?></td>
	<td><?php echo $r["c_opsu"]; ?></td>
	<td><?php echo $r["c_fondo_negro"]; ?></td>
	<td style="width:150px;">
		<a data-id="<?php echo $r["id"];?>" class="btn btn-edit btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["id"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
$("#del-"+<?php echo $r["id"];?>).click(function(e){
			e.preventDefault();
		p = confirm("Estas seguro profesor borrara datos suministrados por el aspirante?");
			if(p){
				$.get("./php/eliminar.php","id="+<?php echo $r["id"];?>,function(data){
					loadTabla();
				});
			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p style="color:black" class="alert alert-warning">No hay Agregados, debes agregar dos veces para mostrar los recaudos</p>
<?php endif;?>

  <script>
  	$(".btn-edit").click(function(){
  		id = $(this).data("id");
  		$.get("./php/formulario.php","id="+id,function(data){
  			$("#form-edit").html(data);
  		});
  		$('#editModal').modal('show');
  	});
  </script>
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Actualizar</h4>
        </div>
        <div class="modal-body">
        <div id="form-edit"></div>
        </div>

      </div>
    </div>
  </div>