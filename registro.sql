create database registro;
use registro;

create table person(
	id int not null auto_increment primary key,
	name varchar(500) not null,
	lastname varchar(500) not null,
	gender varchar(100) not null,
	cedula varchar(100) not null,
	birthdate varchar(255) not null,
  c_partida_nacimiento varchar(100) not null,
  ci_copia varchar(100) not null,
  c_titulo_bachiller varchar(100) not null,
  c_certificacion_calificacione varchar(100) not null,
  c_opsu varchar(100) not null,
  c_fondo_negro varchar(100) not null,
	created_at datetime not null
);