<html>
  <head>
    <title>Elny Burguillos</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
  </head>
  <body>
<div class="container">
<div class="row">
<div class="col-md-12">
<center> <h2>Registro de los aspirantes a nuevo ingreso</h2>

<form class="form-inline" role="search" id="buscar">
      <div class="form-group">
        <input type="text" name="s" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default">&nbsp;<i class="glyphicon glyphicon-search"></i>&nbsp;</button>
  <a data-toggle="modal" href="#newModal" class="btn btn-default">Agregar</a>
    </form>

<br>

  <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="newModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title">Agregar</h4>
        </div>
        <div class="modal-body">
<form role="form" method="post" id="agregar">
  <div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" class="form-control" name="name" value="sas" required>
  </div>
  <div class="form-group">
    <label for="lastname">Apellido</label>
    <input type="text" class="form-control" name="lastname" value="dfsdf" required>
  </div>
   <div class="form-group">
    <label for="gender">Genero</label>
    <input type="text" class="form-control" name="gender" value="dffd" required>
  </div>
   <div class="form-group">
    <label for="cedula">Cedula</label>
    <input type="text" class="form-control" name="cedula" value="dsfef" >
  </div>
 
  <div class="form-group">
    <label for="birthdate">Fecha de Nacimiento</label>
    <input type="text" class="form-control" name="birthdate" value="dfef" >
  </div>

              <h4><b>Docmentos Consignados</b><h4>

    <div class="form-group">
    <label for="c_partida_nacimiento">Entregas copia Partida Nacimiento</label>
   <input type="text" class="form-control" name="c_partida_nacimiento" placeholder="Si o No">
  </div>

     <div class="form-group">
    <label for="ci_copia">Copia C.I.</label>
   <input type="text" class="form-control" name="ci_copia" placeholder="Si o No">
  </div>                    
                            
    <div class="form-group">
    <label for="c_titulo_bachiller">Copia titulo Bachiller</label>
    <input type="text" class="form-control" name="c_titulo_bachiller" placeholder="Si o No">
  </div>     

   <div class="form-group">
    <label for="c_certificacion_calificacione">Copia Certificacion Calificacione</label>
    <input type="text" class="form-control" name="c_certificacion_calificacione" placeholder="Si o No">
  </div>     
 

    <div class="form-group">
    <label for="c_opsu">Copia Ops</label>
     <input type="text" class="form-control" name="c_opsu" placeholder="Si o No">
  </div>   

  <div class="form-group">
    <label for="c_fondo_negro">Copia Fondo Negro</label>
    <input type="text" class="form-control" name="c_fondo_negro" placeholder="Si o No">
  </div>  
  
  <button type="submit" class="btn btn-default"><h4>registrar</h4></button>

</form>
        </div>

      </div>
    </div>
  </div>

<div id="tabla"></div>


</div>
</div>
</div>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script>

function loadTabla(){
  $('#editModal').modal('hide');

  $.get("./php/tabla.php","",function(data){
    $("#tabla").html(data);
  })

}

$("#buscar").submit(function(e){
  e.preventDefault();
  $.get("./php/busqueda.php",$("#buscar").serialize(),function(data){
     alert("Opcion en MANTENIMIENTO! POR FAVOR TRABAJE CON La OPCION AGREGAR, AGREGE DOS VECES PARA MOSTRAR RESLuTADO... Refresque con F5 PARA VOLVER ATRAZ");
    $("#tabla").html(data);
  $("#buscar")[0].reset();
  });
});

loadTabla();


  $("#agregar").submit(function(e){
    e.preventDefault();
    $.post("./php/agregar.php",$("#agregar").serialize(),function(data){
    });
    alert("Agregado exitosamente! Agrege dos veces para mostrar registro");
    $("#agregar")[0].reset();
    $('#newModal').modal('hide');
    loadTabla();
  });
</script>

  </body>
</html>